#!/usr/bin/env python3

from sys import exit, stdout, stderr
if (__name__ == "__main__"):
    try:
        import epmt_settings as settings
    except Exception as e:
        print('\n'+str(e)+'\n', file=stderr)
        exit(1)

    from sys import version_info, argv
    from epmt_cmds import epmt_entrypoint, dump_config
    import argparse
    from io import StringIO
    config_string_file = StringIO()

    # Display help when errors occur without dump_config
    class DefaultHelpParser(argparse.ArgumentParser):
        def error(self, message):
            stderr.write('error: %s\n' % message)
            self.epilog=''
            self.print_help()
            exit(2)
    
    # Generate config variable for epilog on long help
    dump_config(config_string_file)
    parser = DefaultHelpParser(prog='epmt', add_help=True, epilog=config_string_file.getvalue(), description=
        "This is a tool to collect metadata and performance data about an entire job"+
        "\ndown to the individual threads in individual processes."+
        "\nThis tool uses EPMT to perform the process monitoring."+
        "\nThis tool is targeted at batch or ephemeral jobs, not daemon processes.",
        formatter_class=argparse.RawDescriptionHelpFormatter)
    
    # Version
    from epmtlib import version_str
    parser.add_argument('-V', '--version',action='version', version=version_str(), help="Display EPMT Version")
    
    # Global Verbose
    parser.add_argument('-v', '--verbose',action='count', default=0,help="Increase level of verbosity/debug")

    subparser = parser.add_subparsers(title="EPMT Commands", dest="command")
    # Source
    source_parser = subparser.add_parser('source', help="Enable instrumentation of subsequent shell commands",
                                         formatter_class=argparse.RawTextHelpFormatter,
                                         description=
                                         "Source provides commands to begin automatic performance instrumentation of all\n"
                                         "subsequent shell commands. Standard use of this is via the shell's eval method\n"
                                         "inside job scripts or batch system wrappers. For example:\n\n"
                                         "\teval `epmt source` in Bash or Csh\n"
                                         "\teval `epmt source --slurm` for a SLURM prolog.\n\n"
                                         "Two shell functions/aliases are created to pause/restart instrumentation:\n\n\tepmt_uninstrument - to pause automatic instrumentation\n\tepmt_instrument - to renable automatic instruction.\n\n\n"
                                         "**SLURM USERS NOTE** Use in SLURM's prolog, requires a special syntax\n"
                                         "enabled here with the -s or --slurm option. For more info, see:\n"
                                         "https://slurm.schedmd.com/prolog_epilog.html")
    source_parser.add_argument('-s','--slurm', action='store_true',help="Generate shell code for a SLURM prolog, https://slurm.schedmd.com/prolog_epilog.html")
    # Start
    start_parser = subparser.add_parser('start', help="Begin EPMT job data collection",
        description="Start creates directory and metadata file for data collection of a job.")
    start_parser.add_argument('-e', '--error', action='store_true',help="Exit at the first sign of trouble")
    start_parser.set_defaults(epmt_cmd_args=None)
    #    start_parser.add_argument('epmt_cmd_args', nargs='*', help="List of directories from batch", metavar="job_dirs")
    # Stage
    stage_parser = subparser.add_parser('stage', help="Generate job archive",
        description="Stage will compress job or job directories into tgz files for midterm storage then remove \
            original job files and job directory.")
    stage_parser.add_argument('epmt_cmd_args', nargs='*', help="List of directories", metavar="job_dirs")
    stage_parser.add_argument('-e', '--error', action='store_true',help="Exit at the first sign of trouble")
    stage_parser.add_argument('--no-collate', action='store_true',help="Don't collate the files")
    stage_parser.add_argument('--no-compress-and-tar', action='store_true',help="Don't compress and tar the output")
    # annotate
    annotate_parser = subparser.add_parser('annotate', help="Annotate a job", formatter_class=argparse.RawTextHelpFormatter, description="Annotates a job with key/value pairs. This call takes three forms:\n\n"
      "To annotate a stopped job within the batch environment: \n"
      "\tepmt annotate x=100 y=200\n\n"
      "To annotate a stage .tgz file:\n"
      "\tepmt annotate 111.tgz x=100 y=200\n\n"
      "To annotate an existing job in the database:\n"
      "\tepmt annotate 685000 x=100 y=200\n\n"
      "In all the above cases the annotations are *merged* on to the existing annotations, unless --replace is set (in which case existing annotations are removed first)")
    annotate_parser.add_argument('epmt_cmd_args', nargs='+', help="key/value pairs in the form key1=value1 key2=value2..", metavar="kv_pair")
    annotate_parser.add_argument('--replace',action='store_true',default=False,help="Replace existing annotations instead of merging onto them")

    # show functionality is now handled by dump
    # show_parser = subparser.add_parser('show', help="Show details of job from the database",
    #     description="Show will print to console details of a job from the database")
    # show_parser.add_argument('epmt_cmd_args', help="Job ID in database")
    # show_parser.add_argument('-k', '--key', help="Only show the value for the specified key")

    # Dump
    dump_parser = subparser.add_parser('dump', help="Print job metadata from archive or database",
        description="Dump will print to console the metadata of a job directory, job archive or a job in the database")
    dump_parser.add_argument('epmt_cmd_args', default=[], nargs='*', help="Archive or list of archives to dump from file-system, or a list of jobids from database", metavar="job_dir_file_or_id")
    dump_parser.add_argument('-k', '--key', help="Only dump the value for the specified key")

    # db schema
    db_schema_parser = subparser.add_parser('schema', help="Dump the database schema",
        description="schema will dump the database schema")

    # db migrate
    db_migrate_parser = subparser.add_parser('migrate', help="Migrate the database schema",
        description="Migrate the database schema (if needed). It is safe to use this option on an already migrated database")

    # csv convert
    csv_convert_parser = subparser.add_parser('convert', help="Convert csv from v1 to v2 format",
        description="Convert will convert the CSV files inside a .tgz staged file from v1 to v2 format")
    csv_convert_parser.add_argument('src_tgz', action="store", help="Source .tgz file")
    csv_convert_parser.add_argument('dest_tgz', action="store", nargs='?', help="Target .tgz file to create (optional). If not specified, the src .tgz file will be overwritten. Caution!")

    # Run
    run_parser = subparser.add_parser('run', help="Execute a process",
        description="Run will execute a command in the shell, typically used with the auto -a flag to perform metadata collection before and after instrumentation.")
    run_parser.add_argument('-a', '--auto',action='store_true',help="Perform epmt start/epmt stop before/after running")
    run_parser.add_argument('-n', '--dry-run', action='store_true',help="Don't execute anything, just show the commands")
    run_parser.add_argument('epmt_cmd_args', default=[], nargs='+', help="Command to run, **must** be preceded with double dash followed by a space if options are used, i.e. epmt run -- ls -C ", metavar="cmd_line")
    run_parser.set_defaults(dry_run=False, wrapit=False)
    # Stop
    stop_parser = subparser.add_parser('stop', help="Stop EPMT job data collection",
                                       description = "Stop appends additional metadata created during start at job finish time.")
    stop_parser.add_argument('-e', '--error', action='store_true',help="Exit at the first sign of trouble")
    stop_parser.set_defaults(epmt_cmd_args=None)
    # Daemon
    daemon_parser = subparser.add_parser('daemon', help="Run the EPMT daemon",
        description = "daemon runs an EPMT daemon that will periodically perform certain actions")
    daemon_parser.add_argument('-s', '--start',action='store_true',help="Start the EPMT daemon")
    daemon_parser.add_argument('-S', '--stop',action='store_true',help="Stop the EPMT daemon")
    daemon_parser.add_argument('-D', '--foreground',action='store_true',help="Start the daemon in foreground")
    daemon_parser.add_argument('--post-process',action='store_true',help="Perform post-processing and analysis on unprocessed jobs")
    daemon_parser.add_argument('--no-analyze',action='store_true',help="Do not perform analyses on unprocessed jobs, requires --post-process")
    daemon_parser.add_argument('--retire',action='store_true',help="Perform data retirement based on the retention policy in settings")
    daemon_parser.add_argument('-i', '--ingest',nargs='?', default=False, const=settings.stage_command_dest, help="Perform ingestion into the database from the specified directory. If no path is specified the staging destination directory ({}), specified in settings, will be used. See also, --keep and --recursive".format(settings.stage_command_dest), metavar='ingest_path')
    daemon_parser.add_argument('-k', '--keep', default = False, action='store_true', help='Do not remove file on successful ingest. Only meaningful with --ingest')
    daemon_parser.add_argument('-r', '--recursive', default = False, action='store_true', help='Recurse into sub-directories to find staged files. Only meaningful if --ingest is set')
    daemon_parser.set_defaults(start_daemon=False, stop_daemon=False, foreground=False, post_process=False, no_analyze=False,retire=False)
    # shell
    shell_parser = subparser.add_parser('shell', help = "Start an interactive IPython shell, see 'epmt python'",
       description = "shell runs an interactive IPython shell")
    # python
    py_parser = subparser.add_parser('python', help = "Run a Python script under the python interpreter",
       description = "python runs an executable python script under the python interpreter. If no script is provided as an argument, an interactive python shell is executed. If you want an IPython shell, try 'epmt shell'")
    py_parser.add_argument('epmt_cmd_args', nargs='?', help="python script to execute", metavar="script")
    # ui
    gui_parser = subparser.add_parser('gui', help = "Run the EPMT dashboard GUI",
       description = "gui a web-based dashboard accessible at: http://localhost:8050")
    # unittest
    unit_test_parser = subparser.add_parser('unittest', help="Run EPMT unit test suite",
        description = "runs unit tests")
    unit_test_parser.add_argument('epmt_cmd_args', default=[], nargs='*', help='Specific unittest to run')
    # integration tests
    integration_test_parser = subparser.add_parser('integration', help="Run EPMT integration tests",
        description = "runs integration tests")
    integration_test_parser.add_argument('epmt_cmd_args', default=[], nargs='*', help='Specific unittest to run')
    integration_test_parser.add_argument('-x','--exclude', default=[], nargs='*', help='unittest to exclude')
    # drop
    drop_parser = subparser.add_parser('drop', help="Drop the entire database, Caution!! Irreversible!")
    drop_parser.add_argument('-f', '--force', action='store_true',help="Do not prompt to confirm")
    # retire
    retire_parser = subparser.add_parser('retire', help="Delete jobs/models, retention policy in settings.py")
    # Submit
    submit_parser = subparser.add_parser('submit', help="Submit/ingest job into database",
        description="Submit accepts job directories and updates the database configured with \
            directories given.  When run with -n submit will not touch the database and displays the commands leading \
                up to submission.")
    submit_parser.add_argument('epmt_cmd_args', nargs='*', help="List of job .tgz files or directories", metavar="job_or_dir")
    submit_parser.add_argument('-e', '--error', action='store_true',help="Exit at the first sign of trouble")
    submit_parser.add_argument('--remove', action='store_true',help="Remove the job .tgz on successful submission")
    submit_parser.add_argument('-n', '--dry-run', action='store_true',help="Don't touch the database")
    submit_parser.add_argument('--drop',action='store_true',help="Drop all tables/data and recreate before submission")
    from epmtlib import suggested_cpu_count_for_submit
    optimal_cpus = suggested_cpu_count_for_submit()
    submit_parser.add_argument('-p', '--num-cpus', type=int, nargs='?', default=1, const=optimal_cpus, help='Number of parallel processes to use for submission. If this option is selected, but no value is specified, then the optimal value ({0}) will be used, based on your hardware resources. Note: This option is only supported for SQLAlchemy ORM at present.'.format(optimal_cpus))

    # DBSize
    dbsize_parser = subparser.add_parser('dbsize', help="Find detailed size of database",
        description="Prints the on-disk size of different constructs in the database. Not supported for all databases.")
    dbsize_parser.add_argument('epmt_cmd_args', default='all', const='all', nargs='?', choices=["all", "database","table","index","tablespace"], metavar="dbitem", help='%(choices)s')
    
    #dbsize_parser.add_argument('--bytes',action='store_true',help="Output size in bytes")
    #dbsize_parser.add_argument('--json',action='store_true',help="Format output in JSON")
    # Check
    check_parser = subparser.add_parser('check', help='Verify EPMT Installation',
        description="Check will verify basic epmt configuration and functionality.")
    # Delete
    delete_parser = subparser.add_parser('delete', help='Delete jobs from the database',
        description="Deletes jobs from the database.")
    delete_parser.add_argument('epmt_cmd_args', nargs='+', help="job IDs to delete", metavar="jobids")
    # List
    list_parser = subparser.add_parser('list', help='Display all jobs in the database',
        description="Display all jobs in the database")
    list_parser.add_argument('epmt_cmd_args', nargs='*', help="Either keyword: jobs, unprocessed_jobs, unanalyzed_jobs, refmodels, procs/processes, thread_metrics, op_metrics, job_proc_tags or list job IDs to search for", metavar="jobids")
    # Notebook
    notebook_parser = subparser.add_parser('notebook', help="Start iPython Notebook environment",
                                           description="Perform programmatic, interactive analyses of EPMT data in iPython")
    notebook_parser.add_argument('epmt_cmd_args', default=[], nargs='*', 
                                 help="Arguments to ipython notebook, **must** be preceded with double dash followed by a space, i.e. epmt notebook -- --ip 0.0.0.0 --allow-root, see epmt notebook -- --help for more info", metavar="notebook_args")
    # experiment explore (mostly GFDL-specific)
    exp_explore_parser = subparser.add_parser('explore', help="Explore a particular experiment, looking for outliers", description="Simple drilldown workflow into an experiment")
    exp_explore_parser.add_argument('epmt_cmd_args', help="Experiment name, matched against the 'exp_name' in job tags", metavar="exp_name")
    exp_explore_parser.add_argument('--metric', help="metric to measure (default: duration)")
    exp_explore_parser.add_argument('--limit', type=int, help="number of components to show (default: 10)")

# Dummy Help for ./epmt help
    help_parser = subparser.add_parser('help', help="Display help with extended configuration information")
    help_parser.add_argument('api', nargs='*', help="Display help on one or more API functions. If none given then an index of available API functions will be printed")

    # Print help without dump config
    if len(argv) == 1:
        parser.epilog=''
        parser.print_help()
        exit(1)
    args = parser.parse_args()
    #print(vars(args))

    # Print help if epmt help is called
    if args.command == 'help':
        if not args.api:
            parser.print_help()
        else:
            from epmt_cmd_help import epmt_help_api
            # skip the first api string in list and pass the
            # rest (function names) to the function
            epmt_help_api(args.api[1:])
        exit(0)

    # Print help if 
    if args.command == None:
        parser.print_help()
        exit(0)

    retval = epmt_entrypoint(args)
    # trap any unhandled exceptions
    # try:
    #     retval = epmt_entrypoint(args)
    # except Exception as e:
    #     retval = -1
    #     # print('An exception occured (stack backtrace follows below): ', file=stderr)
    #     # from logging import getLogger
    #     # logger = getLogger(__name__)
    #     # logger.error(e, exc_info=True)
    #     print('\n\nAn exception occurred; the full stack backtrace is reproduced below: ', file=stderr)
    #     # sometimes even traceback gives unicode exceptions, so best to be safe
    #     # try:
    #     import traceback
    #     print('-'*60, file=stderr)
    #     traceback.print_exc(file=stderr)
    #     print('-'*60, file=stderr)
    #     # except:
    #     #     pass
    exit(retval)

else:
    print("ERROR: This module cannot be imported.",file=stderr)
    exit(1)
