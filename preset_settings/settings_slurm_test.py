orm = 'sqlalchemy'
db_params = { 'url': 'sqlite:///:memory:', 'echo': False }
bulk_insert = True
epmt_output_prefix = "/tmp/epmt/"
install_prefix = "/opt/epmt/papiex-epmt-install/"
